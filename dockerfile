FROM gcc:latest as build

WORKDIR /app

# RUN apt-get update && apt upgrade && \
#     apt install sqlite3 cmake

RUN apt-get update && apt-get -y install cmake

ADD ./src /app/src

RUN cmake src && make

FROM ubuntu:latest

# apt install sqlite3

#RUN groupadd -r sample && useradd -r -g sample sample
#USER sample

WORKDIR /app

ADD ./py /app/py

COPY --from=build /app/bin/server .

ENTRYPOINT ["./server"]