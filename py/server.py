import socket
from main import Socket

import signal
import sys

def signal_handler(sig, frame):
    global_s.close()
    print('You pressed Ctrl+C!')
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)


HOST = '0.0.0.0'  # Standard loopback interface address (localhost)
PORT = 8084       # Port to listen on (non-privileged ports are > 1023)

global_s = None

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    global_s = s
    s.bind((HOST, PORT))
    s.listen()
    while True:
        conn, addr = s.accept()
        with conn:
            socket_ = Socket()
            print(f'{addr} connected')
            while True:
                data = conn.recv(1024)
                if not data:
                    break
                
                conn.sendall(socket_.handle(data))
            print(f'{addr} disconnected')