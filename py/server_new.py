import os, socket
import signal
import sys
from main import Socket

host="0.0.0.0"
port=8084

def signal_handler(sig, frame):
    s.close()
    print('You pressed Ctrl+C!')
    #sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

s=socket.socket()
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((host, port))
s.listen(3)

while True:
    conn, addr=s.accept()
    child_pid=os.fork()
    if child_pid==0:
        socket_ = Socket()
        print(f'{addr} connected')
        while True:
            data = conn.recv(10000)
            if not data:
                break
            
            conn.sendall(socket_.handle(data))
        #print(f'{addr} disconnected')
        sys.exit(0)