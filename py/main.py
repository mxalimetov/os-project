
import json

from models import User, Job, Resume, document2dict, Application

def error(err_str, **kwargs):
    return {"ok":False, "err_str":err_str, **kwargs}

def ok(result, **kwargs):
    return {"ok":True, "result":result, **kwargs}

class Socket:
    def __init__(self):
        self.user = None

    def handle(self, data):
        input_ = data.decode()
        try:
            input_ = json.loads(input_)
        except ValueError:
            pass
        output = self.handle_data(input_)
        print(output)
        return json.dumps(output).encode()
    
    def handle_data(self, obj):
        #print(obj)
        if not isinstance(obj, dict): return error("please provide object")
        if "method" not in obj: return error("please provide method")
        method = obj.pop("method")
        try:
            result = self.handle_method(method, obj)
        except Exception as e:
            return error(str(e), method=method)

        return {"ok":True, "result":result, "method":method}
    
    def handle_method(self, method, data):
        #print(method, data)
        if method=='logout':
            self.user = None
            return True
        elif method=='is_logged_in':
            if self.user: return self.user.role
            return None

        if self.user==None:
            return unauthorized_service.handle(self, method, data)

        return self.authorized_handle(method, data)
                
    
    def authorized_handle(self, method, data):
        print(self.user.email, method, data)
        if self.user.role=='admin':
            return admin_service.handle(self.user, method, data)
        elif self.user.role=='employer':
            return employer_service.handle(self.user, method, data)
        elif self.user.role=='employee':
            return employee_service.handle(self.user, method, data)
        
        raise Exception("role not found")

class UnauthorizedService:
    def handle(self, socket, method, data):
        if hasattr(self, f"method_{method}"):
            return getattr(self, f"method_{method}")(socket, **data)
        raise Exception("method not found")

    def method_login(self, socket, email, password):
        users = User.objects(
            email=email,
            password=password,
        )
        if len(users)>0:
            socket.user = users[0]
            return socket.user.role
        raise Exception("user not found")
        
    def method_register(self, socket, email, password, role):
        user = User(
            email=email,
            password=password,
            role=role,
        )
        user.save()
        socket.user = user
        return True

class AdminService:
    def handle(self, user, method, data):
        pass

class EmployerService:
    def handle(self, user, method, data):
        if hasattr(self, f"method_{method}"):
            return getattr(self, f"method_{method}")(user, **data)
        raise Exception("method not found")

    def method_create_job(self, user, **kwargs):
        job = Job(**kwargs)
        job.created_by = user
        job.save()
        return True
    
    def method_read_jobs(self, user, **kwargs):
        if "query" in kwargs:
            return [document2dict(x) for x in Job.objects(created_by=user, title__icontains=kwargs['query'])]
        return [document2dict(x) for x in Job.objects(created_by=user)]
    
    def method_read_resumes(self, user, **kwargs):
        if "query" in kwargs:
            return [document2dict(x) for x in Resume.objects(skills__contains=kwargs['query'])]
        return [document2dict(x) for x in Resume.objects()]
    
    def method_delete_job(self, user, id):
        job = Job.objects(id=id).get()
        if job.created_by != user:
            raise Exception('this does not belong to you')
        job.delete()
        return True
    
    def method_read_job(self, user, id):
        # jobs = Job.objects(id=id)
        # if len(jobs)>0:
        #     return document2dict(jobs[0])
        job = Job.objects(id=id).get()
        return document2dict(job)
    
    def method_read_applications(self, user):
        jobs = Job.objects(created_by=user)
        return [document2dict(x) for x in Application.objects(job__in=jobs)]

class EmployeeService:
    def handle(self, user, method, data):
        if hasattr(self, f"method_{method}"):
            return getattr(self, f"method_{method}")(user, **data)
        raise Exception("method not found")
    
    def method_apply(self, user, job_id):
        job = Job.objects.get(id=job_id)
        if not job:
            raise Exception("job not found")

        application = Application(
            applicant=user,
            job=job
        )
        application.save()
        return True

    def method_read_jobs(self, user, **kwargs):
        if "query" in kwargs:
            return [document2dict(x) for x in Job.objects(title__icontains=kwargs['query'])]
        return [document2dict(x) for x in Job.objects()]
    
    def method_read_resumes(self, user, **kwargs):
        if "query" in kwargs:
            return [document2dict(x) for x in Resume.objects(created_by=user, skills__contains=kwargs['query'])]
        return [document2dict(x) for x in Resume.objects(created_by=user)]
    
    def method_read_applications(self, user):
        return [document2dict(x) for x in Application.objects(applicant=user)]
    
    def method_create_resume(self, user, **kwargs):
        resume = Resume(**kwargs)
        resume.created_by = user
        resume.save()
        return True
    


admin_service = AdminService()
employer_service = EmployerService()
employee_service = EmployeeService()
unauthorized_service = UnauthorizedService()