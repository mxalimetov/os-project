
import socket
import json

HOST = '127.0.0.1'
PORT = 8084

class Client:
    def __init__(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((HOST, PORT))
    
    def send(self, data):
        self.socket.sendall(data.encode())
        return self.socket.recv(5000).decode()
    
    def send_data(self, data):
        return json.loads(self.send(json.dumps(data)))
    
    def send_method(self, method, **data):
        return self.send_data({"method":method, **data})

    # def __del__(self):
    #     self.socket.close()

client = Client()

#print(client.send_method("login", email='vecume@gmail.com', password='12345678'))

# employer
#print(client.send_method("login", email='asd', password='asd'))

#print(client.send_method("register", email='sdfg', password='sdfg', role='employee'))
# employee
#print(client.send_method("login", email='sdfg', password='sdfg'))
#print(client.send_method("login", email='vecume@gmail.com', password='12345678'))
# client.send_method("test", asd='asd')
#print(client.send_method("create_job", title='test title'))
#print(client.send_method("create_job", title='test title', description='asdasd'))
#print(client.send_method("read_jobs"))
#print(client.send_method("apply", job_id='61b4af1140b7664df1f0e6a8'))
#print(client.send_method("read_applications",))
#print(client.send_method("search_jobs",query='test'))
#print(client.send_method("delete_job",id='61b4a18a265fb3bb123dbc4b'))
#print(client.send_method("read_job",id='61b4a0ea029c17b9933dbc41'))
#print(client.send_method("read_jobs",))
#print(client.send_method("read_applications",))


#print(client.send_method("login", username='asd', password='asd'))

# with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
#     s.connect((HOST, PORT))
#     s.sendall(b'Hello, world')
#     data = s.recv(1024)

# print('Received', repr(data))