
from mongoengine import Document, StringField, DateTimeField, IntField, connect
from mongoengine import ReferenceField
import datetime

#connect(host="mongodb://my_user:my_password@hostname:port/my_db?authSource=admin")
#connect(host="mongodb://127.0.0.1/test?authSource=admin")
connect('test')
import json
def document2dict(doc):
    return json.loads(doc.to_json())

class User(Document):
    email = StringField(max_length=200, required=True)
    password = StringField(max_length=200, required=True)
    role = StringField(max_length=200, required=True)

    contact = StringField()

class Job(Document):
    # salary, created time, created by, applications, company 
    title = StringField(max_length=200, required=True)
    description = StringField()
    salary = IntField()
    location = StringField(max_length=200)
    company_name = StringField()
    date_created = DateTimeField(default=datetime.datetime.utcnow)
    created_by = ReferenceField(User)

class Resume(Document):
    name = StringField(max_length=200, required=True)
    surname = StringField()
    experience = StringField()
    skills = StringField()
    location = StringField()
    date_created = DateTimeField(default=datetime.datetime.utcnow)
    created_by = ReferenceField(User)

class Application(Document):
    # salary, created time, created by, applications, company 
    applicant = ReferenceField(User)
    job = ReferenceField(Job)
    date = DateTimeField(default=datetime.datetime.utcnow)


#User(username="asd", password="asd", role="asd").save()