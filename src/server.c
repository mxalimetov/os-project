#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define PORT 8080
#define EXIT_FAILURE 1

PyObject* pModule;
//PyObject* pSocketClass;

void server_handle(int socket_descriptor) {
    //printf("Handling request\n");
    PyObject* pSocketClass = PyObject_GetAttrString(pModule, (char*) "Socket");

    PyObject* pSocketObject = PyObject_CallObject(pSocketClass, PyTuple_Pack(0));

    PyObject* pHandle = PyObject_GetAttrString(pSocketObject, (char*) "handle");

    while(1) {

        int valread;
        char pRequestBytes[1024];
        valread = read(socket_descriptor, pRequestBytes, 1024);
        if(valread==0) break;
        pRequestBytes[valread] = 0;

        printf("\nRequest: %s\n", pRequestBytes);

        PyObject* pRequest = PyBytes_FromString(pRequestBytes);
        printf("step1\n");
        PyObject* pResponse = PyObject_CallObject(pHandle,
            PyTuple_Pack(1, pRequest)
        );
        printf("step2\n");
        char* pResponseBytes = PyBytes_AsString(pResponse);

        printf("\nResponse: %s\n", pResponseBytes);

        send(socket_descriptor, pResponseBytes, strlen(pResponseBytes), 0);
    }
}
void server_accept(int socket_descriptor) {
    struct sockaddr_in address;
    int addrlen = sizeof(address);
    int new_socket_descriptior, valread;
    printf("Waiting for request\n");
    if ((new_socket_descriptior = accept(socket_descriptor, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    if (fork() == 0) {
        server_handle(new_socket_descriptior);
        exit(0);
    } else {
        close(new_socket_descriptior);
    }
    return;
}

void server_socket() {
    int socket_descriptor;
    if ((socket_descriptor = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket");
        exit(EXIT_FAILURE);
    }
       
    int opt = 1;
    if (setsockopt(socket_descriptor, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
       
    if (bind(socket_descriptor, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind");
        exit(EXIT_FAILURE);
    }

    if (listen(socket_descriptor, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    printf("Listening for connections\n");
    while(1) {
        server_accept(socket_descriptor);
    }
}
void init_py() {
    PyObject* pSys = PyImport_ImportModule( "sys" );
    PyObject* pSysPath = PyObject_GetAttrString( pSys, "path" );
    PyObject* pFolderPath = PyUnicode_FromString( "./py/" );
    PyList_Append( pSysPath, pFolderPath );
    pModule = PyImport_ImportModule("main");
    //pSocketClass = PyObject_GetAttrString(pModule, (char*) "Socket");
    
}
int main(int argc, char **argv) {
    Py_Initialize();
    init_py();
    server_socket();
    Py_Finalize();
    return 0;
}
